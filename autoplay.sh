#!/bin/sh
# autoplay "Primeclime"

# dice = 1..maxdc
maxdc=6
# limit for signum detection: below siglim means positive step
siglim=3
# winning limit
winlim=100

# simple expensive random number generator
dice(){
 local dx
 dx=`uuidgen|tr -c -d '0-9'|sed -e "s/^0\*//;s:\$: 0k$$+$maxdc%1+p:"|dc`
 echo ${dx:-0}
#echo ::dx=$dx:: >&2
}

# true if arg.1 divisible by arg.2, i.e, if 0 == arg.1 % arg.2
divck(){
 test 0 -eq `echo "0k $1 $2 %p"|dc`
}

# check for primeness of arg.1 (positive integer)
primck(){
 local i m pr
 case $1 in
# special cases
  0) pr=no ;;
  1) pr=no ;;
  2) pr=yes ;;
# accelerate for single digits
  3) pr=yes ;;
  4) pr=no ;;
  5) pr=yes ;;
  6) pr=no ;;
  7) pr=yes ;;
  8) pr=no ;;
  9) pr=no ;;
  *)
# check for 2 as special case
   if divck $1 2
   then pr=no
   else
    pr=yes
# max.check = sqrt(arg.1)+1
    m=`echo "0k $1 v1+p"|dc`
    i=1
# very stupid algorithm: check division by odd integers
    while test $i -lt $m
    do
     i=`expr $i + 2`
     if divck $1 $i
     then
      pr=no
      break
     fi
    done
   fi
 esac
#echo :$1:$pr: >&2
 test $pr = yes
}

# return next value based on old (arg.1) and dice (arg.2)
walk(){
 local nx ox dv
 ox=${1:-0}
 dv=${2:-0}
 if test $dv -lt 1
 then dv=`dice`
 fi
 echo "	dice=$dv" >&2
 nx=`expr $ox + $dv`
#echo :ox=$ox dv=$dv nx=$nx: >&2
 if primck $nx
 then
# sum of digits of new number
  qs=`echo "0$nx p"|sed -e 's/\([0-9]\)/\1+/g;s/^/0 /'|dc`
#echo :qs $nx = $qs: >&2
  if test $dv -ge $siglim
  then nx=`expr $nx - $qs`
  else nx=`expr $nx + $qs`
  fi
 fi
 echo $nx
}

k=0
i=0
echo :k=$k winlim=$winlim
while test $k -lt $winlim
do k=`walk $k`
 i=`expr $i + 1`
 echo "$i	$k"
done
