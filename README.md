# Prime Climb Game

simple "ladder game" using primes

## Motivation

In children's "ladder/climbing games," players each have a token, placed
on a map of fields. They throw a dice in turns, and advance their tokens
accordingly. Whenever they arrive on a "special field," they either advance
or fall back, depending on the field. (The action most often is described
in comic-strip style directly imprinted on the field.)
The aim is to reach the end of the map/list of fields.

The idea here is to replace the (visual) map by a range of integers, e.g
from 0 to 100, and to use prime numbers like "special fields" and some
numerical character of each prime number to define the "action" to be
done, when the prime number is reached.

_It is nothing more than a small idea I got while watching some kids playing such a "ladder game"._

## Description

Players start each with an initial status value of 0,
then throw a dice or draw numbers in turn, and add them to their status
value. If the new value is *not prime,* nothing further happens. But
if the new status value *is prime,* then it gets incremented by the
sum of its digits if the dice shows a number below 3, or decremented by
the sum of its digits if the dice shows a number equal to or above 3.
Then it's the next player's turn, until the first player reaches a status
value of 100 or higher.

### Pseudocode for one player

1. S:=0
2. if S higher than or equal to 100, then stop (first player reaching wins)
3. throw dice, resulting value D
4. S:=S+D
5. if S not prime, go to step 2
6. Q:=sum of digits of S
7. if D higher than or equal to 3, then K:=-1, else K:=1
8. S:=S+K*Q
9. go to step 2

### Demo implementation

The script `autoplay.sh` simulates one player. It alternatively displays
the step number and the current status value, as well as the dice value
for the next step. Therefore, the first value on the last output line is
the number of steps needed in the simulated game to reach 100 or more.

---

_2016 Y.Bonetti_
